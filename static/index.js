fetch('static/worldl.json')
   .then(r=>r.json())
   .then(r=>{
       for(let a of r){
           let b = document.createElement('div');
           b.innerText = a.name;
           b.classList.add('countryIndex');
           b.onclick = ()=>{
               document.getElementById('content').innerHTML = `
               <h1>${a.name}</h1>
               <table>
               <tr><th>Id: </th><td>${a.id}</td></tr>
               <tr><th>Capital: </th><td>${a.capital}</td></tr>
               <tr><th>Continent: </th><td>${a.continent}</td></tr>
               <tr><th>Area: </th><td>${a.area}</td></tr>
               <tr><th>GDP: </th><td>${a.gdp}</td></tr>
               <tr><th>Population: </th><td>${a.population}</td></tr>
               <tr><td colspan=2><img src=${a.flag}></td></tr>
               </table>
               `;
               
           }
           document.getElementById('countryList').append(b);
       }

       //Implement Alphabet buttons
       for(let button of document.querySelectorAll('#alphabet button')){
           button.onclick = ()=>{
               for(let c of document.querySelectorAll('#countryList div')){
                   if (c.innerText.startsWith(button.innerText)){
                       c.classList.remove('hide');
                   }else{
                       c.classList.add('hide');
                   }
               }
           }
       }
       for(let letter of document.querySelectorAll('#cselect')){
        letter.onchange = ()=>{
            for(let c of document.querySelectorAll('#countryList div')){
                if (c.innerText.startsWith(letter.value)){
                    c.classList.remove('hide');
                }else{
                    c.classList.add('hide');
                }
            }
        }
    }
   })